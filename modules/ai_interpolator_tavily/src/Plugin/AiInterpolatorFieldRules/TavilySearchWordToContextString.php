<?php

namespace Drupal\ai_interpolator_tavily\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator_tavily\TavilySearchWordToContextBase;

/**
 * The rules for a string_long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_tavily_search_to_string_long",
 *   title = @Translation("Tavily Search Word to String"),
 *   field_rule = "string_long",
 * )
 */
class TavilySearchWordToContextString extends TavilySearchWordToContextBase {

}
