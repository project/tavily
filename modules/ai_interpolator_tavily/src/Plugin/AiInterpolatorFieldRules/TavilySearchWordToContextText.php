<?php

namespace Drupal\ai_interpolator_tavily\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator_tavily\TavilySearchWordToContextBase;

/**
 * The rules for a text_long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_tavily_search_to_text_long",
 *   title = @Translation("Tavily Search Word to String"),
 *   field_rule = "text_long",
 * )
 */
class TavilySearchWordToContextText extends TavilySearchWordToContextBase {

  /**
   * {@inheritDoc}
   */
  public function rowBreaker() {
    return "<br />";
  }

}
