<?php

namespace Drupal\ai_interpolator_tavily\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\tavily\TavilyApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The rules for a link field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_tavily_search_to_link",
 *   title = @Translation("Tavily Search Word to Link"),
 *   field_rule = "link",
 * )
 */
class TavilySearchWordToLink extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Tavily Search Word to Link';

  /**
   * The Tavily API.
   */
  public TavilyApi $tavilyApi;

  /**
   * The constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TavilyApi $tavilyApi) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->tavilyApi = $tavilyApi;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('tavily.api')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "{{ context }}";
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $values = [];
    $prompts = parent::generate($entity, $fieldDefinition, $interpolatorConfig);
    foreach ($prompts as $prompt) {
      $results = $this->tavilyApi->search($prompt);
      foreach ($results['results'] as $result) {
        $values[] = [
          'uri' => $result['url'],
        ];
      }
    }
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    // Check so the value is a valid url.
    if (isset($value['uri']) && filter_var($value['uri'], FILTER_VALIDATE_URL)) {
      return TRUE;
    }
    return FALSE;
  }

}
