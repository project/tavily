<?php

namespace Drupal\ai_interpolator_tavily;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\tavily\TavilyApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base for context.
 */
class TavilySearchWordToContextBase extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Tavily Search Word to String';

  /**
   * The Tavily API.
   */
  public TavilyApi $tavilyApi;

  /**
   * The constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TavilyApi $tavilyApi) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->tavilyApi = $tavilyApi;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('tavily.api')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "{{ context }}";
  }

  /**
   * {@inheritDoc}
   */
  public function extraAdvancedFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form['interpolator_tavily_concatenate'] = [
      '#type' => 'checkbox',
      '#title' => 'Concatenate',
      '#description' => $this->t('If checked, the results will be concatenated into one field.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_tavily_concatenate', FALSE),
      '#weight' => 24,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $values = [];
    $prompts = parent::generate($entity, $fieldDefinition, $interpolatorConfig);
    foreach ($prompts as $prompt) {
      $results = $this->tavilyApi->search($prompt);
      foreach ($results['results'] as $result) {
        if ($interpolatorConfig['tavily_concatenate']) {
          if (!isset($values[0])) {
            $values[0] = $result['title'] . $this->rowBreaker() . $result['content'];
          }
          else {
            $values[0] .= $this->rowBreaker() . $this->rowBreaker() . $result['title'] . $this->rowBreaker() . $result['content'];
          }
        }
        else {
          $values[] = $result['title'] . $this->rowBreaker() . $result['content'];
        }
      }
    }
    return $values;
  }

  /**
   * Breaker.
   *
   * @return string
   *   The row breaker.
   */
  public function rowBreaker() {
    return "\n";
  }

}
